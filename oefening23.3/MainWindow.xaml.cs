﻿using System.Windows;

namespace oefening23._3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Click_Toevoegen(object sender, RoutedEventArgs e)
        {
            try
            {
                Werknemer werknemer = new Werknemer(txtAchternaam.Text, txtVoorrnaam.Text, Convert.ToDouble(txtVerdiensten.Text));
                List<Werknemer> werknemers = new List<Werknemer>();
                werknemers.Add(werknemer);
                //txtResultaat.Text += txtAchternaam.Text + txtVoorrnaam.Text.PadLeft(20) + (txtVerdiensten.Text.Replace(".", ",") + "€").PadLeft(20) + Environment.NewLine;
                foreach (Werknemer werknemer1 in werknemers)
                {
                    lstResultaat.Items.Add(werknemer1.VollidgeWeergave);
                }
                txtAchternaam.Text = string.Empty;
                txtVoorrnaam.Text = string.Empty;
                txtVerdiensten.Text = string.Empty;

            }
            catch
            {
                MessageBox.Show("U hoeft of niet alle valden ingevuld of verdiensten is geen numerieke waard!", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }
    }
}