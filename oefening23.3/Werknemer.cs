﻿namespace oefening23._3
{
    internal class Werknemer
    {
        private string _voornaam;
        private string _achternaam;
        private double _verdiensten;

        public Werknemer() { }
        public Werknemer(string achternaam, string voornaam, double verdiensten)
        {
            _voornaam = voornaam;
            _achternaam = achternaam;
            _verdiensten = verdiensten;
        }
        public string Achternaam { get { return _achternaam; } set { _achternaam = value; } }
        public string Voornaam
        {
            get { return _voornaam; }
            set { _voornaam = value; }
        }
        public double Verdiensten { get { return _verdiensten; } set { _verdiensten = value; } }
        public string VollidgeWeergave { get { return _achternaam + _voornaam.PadLeft(10) + _verdiensten.ToString().PadLeft(15) + "€"; } }


    }
}
